const gulp = require('gulp');
const browserify = require('gulp-browserify');
const concat = require('gulp-concat');
const path = require('path');
const fs = require('fs');


const _config = {
    appDir: path.join(__dirname, 'app'),
    entry: path.join(__dirname, 'app/index.js'),
    demoDest: path.join(__dirname, 'demo'),
    dest: path.join(__dirname, 'dest'),
    destFileName: 'bundle.js',
}

var toggleGlobalVariable = (doAdd) => {
    const global = 'module.exports = window.Intuer';
    const notGlobal = 'module.exports';

    const toToggle = doAdd ?
        [notGlobal, global] : [global, notGlobal];

    let indexFileStr = fs.readFileSync(_config.entry, { encoding: 'utf8' });

    indexFileStr = indexFileStr.replace(toToggle[0], toToggle[1]);
    fs.writeFileSync(_config.entry, indexFileStr, { encoding: 'utf8' });
};

gulp.task('default', () => {
    toggleGlobalVariable(true);

    gulp.src(_config.entry)
        .pipe(browserify())
        .pipe(concat(_config.destFileName))
        .pipe(gulp.dest(_config.dest))
        .pipe(gulp.dest(_config.demoDest))
        .on('end', toggleGlobalVariable);

    console.log('Bundling and copying to:');
    console.log(`- ${_config.dest}`);
    console.log(`- ${_config.demoDest}`);
});
