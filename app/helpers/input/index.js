const keyToCode = require('./keyToCode');

module.exports = class Input {
    constructor(name) {
        this.name = name;
    }
    
    byKey(key) {
        key = key.toLowerCase();
        this.key = keyToCode[key];
        if (!this.key) {
            throw 'Unknown key: ' + key;
        }
        return this;
    }

    byKeyCode(code) {
        this.keyCode = code;
        return this;
    }

    withShift() {
        this.modifier = 'shiftKey';
        return this;
    }
    
    withCtrl() {
        this.modifier = 'ctrlKey';
        return this;
    }
    
    withAlt() {
        this.modifier = 'altKey';
        return this;
    }

    withMeta() {
        this.modifier = 'metaKey';
        return this;
    }

    on(el) {
        this.el = el;
        return this;
    }

    listen(fn) {
        const key = this.keyCode || this.key;

        if (key) {
            this.listener = this.modifier ?
                (e) => {
                    if (e.which === key && e[this.modifier]) fn(e);
                } :
                (e) => {
                    if (e.which === key) fn(e);
                }
        } else {
            this.listener = this.modifier ?
                (e) => {
                    if (e[this.modifier]) fn(e);
                } :
                fn;
        }

        this.el.addEventListener(this.name, this.listener);

        return this;
    }

    stopListening() {
        this.el.removeEventListener(this.name, this.listener);
    }
}
