const ajax = require('./utils/ajax.js');
const identifier = require('./utils/identifier.js');
const objectUtils = require('./utils/objectUtils.js');
const commonUtils = require('./utils/common.js');
const uiUtils = require('./utils/ui.js');
const selectors = require('./utils/view/selectWrap.js');
const event = require('./utils/event.js');
const Deferred = require('./utils/deferred');
const WaitUntil = require('./utils/waitUntil.js');
const arrayUtils = require('./utils/arrayUtils.js');
const fill = require('./view/fill.js');
const { Storage } = require('./model/storage');
const Input = require('./helpers/input');
const Module = require('./module');
const App = require('./app');

class Intuer {
    constructor() {
        this.ajax = ajax;

        this.isObject = identifier.isObject;
        this.isArray = identifier.isArray;
        this.isString = identifier.isString;
        this.isNumber = identifier.isNumber;
        this.isFunction = identifier.isFunction;
        this.isEnumerable = identifier.isEnumerable;
        this.isUndefined = identifier.isUndefined;
        this.isHtmlElement = identifier.isHtmlElement;
        this.isNull = identifier.isNull;

        this.each = commonUtils.each;
        this.find = commonUtils.find;
        this.filter = commonUtils.filter;
        this.contains = commonUtils.contains;
        this.map = commonUtils.map;
        this.replaceEmpty = commonUtils.replaceEmptyObjectsAndArrays;

        this.fill = fill;
        this.Event = event.Event;
        this.Deferred = Deferred;
        this.eventQueue = event.queue;
        this.waitUntil = new WaitUntil();

        this.object = objectUtils;
        this.array = arrayUtils;

        this.ui = uiUtils;

        this.select = selectors.select;
        this.selectUp = selectors.selectUp;
        this.Storage = Storage;
        this.Input = Input;

        this.App = App;
        this.Module = Module;
    }
};

module.exports = new Intuer();
