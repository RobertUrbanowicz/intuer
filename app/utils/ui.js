const arrayUtils = require('./arrayUtils');
const id = require('./identifier');
const { each } = require('./common');

module.exports = {
    toggleClass: toggleClass,
    switchClasses: switchClasses,
    removeClass: removeClass,
    addClass: addClass,
    animate: animate,
}

function toggleClass(el, cssClass) {
    if ( ! id.isObject(el) ) throw 'First argument must be a DOM element. Got ' + el;
    if ( ! cssClass ) throw 'Second argument is required.';

    let doAdd = true;
    const classList = el.className.split(' ');
    const classListCopy = [];

    for (let i = 0; i < classList.length; i++) {
        if (classList[i] === cssClass) {
            doAdd = false;
        } else {
            classListCopy.push(classList[i]);
        }
    }

    if (doAdd) {
        classListCopy.push(cssClass);
    }

    el.className = classListCopy.join(' ');

    return classListCopy;
}

function switchClasses(el, cssClass1, cssClass2) {
    if ( ! id.isObject(el) ) throw 'First argument must be a DOM element. Got ' + el;
    if ( ! cssClass1 ) throw 'Second argument is required.';
    if ( ! cssClass2 ) throw 'Third argument is required.';

    let classToAdd, classListCopy;
    var classList = el.className.split(/\s+/);

    for (let i = 0; i < classList.length; i++) {
        if (classList[i] === cssClass1) {
            removeBothClasses();
            classToAdd = cssClass2;
            break;
        } else if (classList[i] === cssClass2) {
            removeBothClasses();
            classToAdd = cssClass1;
            break;
        }
    }

    function removeBothClasses() {
        classListCopy = arrayUtils.removeElement(classList, cssClass1);
        classListCopy = arrayUtils.removeElement(classListCopy, cssClass2);
    }

    if (classToAdd) {
        classListCopy.push(classToAdd);
        el.className = classListCopy.join(' ');
        return classListCopy;
    } else {
        return el.className;
    }
}

function removeClass(el, cssClass) {
    if ( ! id.isObject(el) ) throw 'First argument must be a DOM element. Got ' + el;

    const classList = arrayUtils.removeElement(el.className.split(' '), cssClass);
    el.className = classList.join(' ');
    return classList;
}

function addClass(el, cssClass) {
    if ( ! id.isObject(el) ) throw 'First argument must be a DOM element. Got ' + el;

    const classList = arrayUtils.removeElement(el.className.split(' '), cssClass);
    classList.push(cssClass);
    el.className = classList.join(' ');
    return classList;
}


class DefaultAnimateConfig {
    constructor() {
        this.element = null;
        this.properties = {};
        this.duration = 200;
        this.cb = () => {};
        this.fps = 60;
    }
}

/**
 * @param {DefaultAnimateConfig} cfg
 */
function animate(cfg) {
    if (!cfg.element) throw "Element to animate is required";

    cfg = Object.assign(new DefaultAnimateConfig(), cfg);

    each(cfg.properties, (propValue, propName) => {
        const currentValueUnitMatch = cfg.element.style[propName].match(/[a-zA-Z%]+/);
        const targetValueUnitMatch = propValue.match(/[a-zA-Z%]+/);
        const unit = targetValueUnitMatch ? targetValueUnitMatch[0] : currentValueUnitMatch ? currentValueUnitMatch[0] : '';

        let currentValue = parseFloat(cfg.element.style[propName]) || 0;
        propValue = parseFloat(propValue);

        const valueDiff = propValue - currentValue;

        const animationInterval = Math.round(1000 / cfg.fps);

        let frames = Math.round(cfg.duration / animationInterval);
        frames = frames || 1;

        const stepValue = valueDiff / frames;

        let currentFrame = 0;

        const intervalId = window.setInterval(changeAttr, animationInterval);

        function changeAttr() {
            currentFrame ++;

            if (currentFrame === frames) {
                window.clearInterval(intervalId);
                cfg.element.style[propName] = `${propValue}${unit}`;
                cfg.cb();
            } else {
                currentValue = currentValue + stepValue;

                window.requestAnimationFrame(() => {
                    cfg.element.style[propName] = `${currentValue}${unit}`;
                });
            }
        }

    });
}