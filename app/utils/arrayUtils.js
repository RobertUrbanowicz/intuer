const { isArray } = require('./identifier');

module.exports = {
    removeElement: removeElement,
    mapToObject: mapToObject,
};


function removeElement(srcArray, elementToRemove) {
    if ( ! isArray(srcArray) ) throw 'First argument must be an array. Got ' + srcArray;
    if ( ! elementToRemove ) throw 'Second argument is required';
    const resultArray = [];
    srcArray.forEach(item => {
        if (item !== elementToRemove) {
            resultArray.push(item);
        }
    });
    return resultArray;
}

function mapToObject(array, fn = (value, key) => { return { key: key, value: value } }) {
    const result = {};

    array.forEach((value, key) => {
        const item = fn(value, key);
        result[item.key] = item.value;
    });

    return result;
}
