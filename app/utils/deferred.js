const id = require('./identifier');

module.exports = class Deferred {
    constructor() {
        this.resolved = false;
        this.rejected = false;
        this.fulfillData = null;
        this.resolveListeners = [];
        this.rejectListeners = [];
        this.alwaysListeners = [];

        this.promise = {
            onResolve: (fn) => {
                if (this.resolved) {
                    this._callListener(fn, this.fulfillData);
                }
                else if (!this.rejected) {
                    this.resolveListeners.push(fn);
                }

                return this.promise;
            },
        
            onReject: (fn) => {
                if (this.rejected) {
                    this._callListener(fn, this.fulfillData);
                }
                else if (!this.resolved) {
                    this.rejectListeners.push(fn);
                }

                return this.promise;
            },
        
            always: (fn) => {
                if (this.resolved || this.rejected) {
                    this._callListener(fn, this.fulfillData);
                } else {
                    this.alwaysListeners.push(fn);
                }

                return this.promise;
            },
        
            then: (onResolve, onReject, always) => {
                if (onResolve) this.promise.onResolve(onResolve);
                if (onReject) this.promise.onReject(onReject);
                if (always) this.promise.always(always);

                return this.promise;
            },
        };
    }

    resolve(...args) {
        this._fulFill(args, 'resolved', 'resolveListeners');
        return this.promise;
    }
    
    reject(...args) {
        this._fulFill(args, 'rejected', 'rejectListeners');
        return this.promise;
    }

    _fulFill(args, prop, listenersProp) {
        if (this.resolved || this.rejected) {
            console.warn('This promise is already fulfilled');
            return;
        }

        this[prop] = true;
        this.fulfillData = args;

        this._callListeners(this[listenersProp], args);
        this._callListeners(this['alwaysListeners'], args);
        this._cleanUp();
    }

    _callListeners(listeners, args) {
        listeners.forEach(fn => this._callListener(fn, args));
    }

    _callListener(fn, args) {
        let returnedData = fn(...args);
        returnedData = id.isArray(returnedData) ? returnedData : [returnedData];

        returnedData.forEach((arg, index) => {
            if (!id.isUndefined(arg))
                args[index] = arg;
        });
    }
    
    _cleanUp() {
        delete this.resolveListeners;
        delete this.rejectListeners;
        delete this.alwaysListeners;
    }
}
