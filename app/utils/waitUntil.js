var isFunction = require('../utils/identifier').isFunction;
var each = require('../utils/common').each;

module.exports = WaitUntil;

function WaitUntil() {
    this.events = {};

    this.trigger = function(eventName, data) {
        if (this.events[eventName]) {
            this.events[eventName].trigger(data);
            this.events[eventName].triggered = true;
        } else {
            this.events[eventName] = new WaitUntilEvent(true);
        }
    }

    this.setHandler = function(eventName, handler) {
        if (!this.events[eventName]) {
            this.events[eventName] = new WaitUntilEvent();
        }
        this.events[eventName].addHandler(handler);
    }

    this.destroy = function(name) {
        if (this.events[name]) delete this.events[name];
        else console.error('Can not find event with name ' + name);
    }
}

function WaitUntilEvent(triggered) {
    this.triggered = triggered ? true : false;
    this.handlers = [];

    this.addHandler = function(handler) {
        if (!isFunction(handler)) throw handler + ' is not a function.';
        if (this.triggered) handler(this.data);
        else this.handlers.push(handler);
    }

    this.trigger = function(data) {
        this.data = data;
        each(this.handlers, function(handler) {
            handler(data);
        });
        delete this.handlers;
    }
}
