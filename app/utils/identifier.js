module.exports = {
    isObject: isObject,
    isArray: isArray,
    isString: isString,
    isNumber: isNumber,
    isFunction: isFunction,
    isHtmlElement: isHtmlElement,
    isEnumerable: isEnumerable,
    isNull: isNull,
    isUndefined: isUndefined,
}


function isObject(el) {
    return typeof el === 'object';
}

function isArray(el) {
    return el && el.constructor === window.Array ? true : false;
}

function isString(el) {
    return typeof el === 'string';
}

function isNumber(el) {
    return typeof el === 'number' && !Number.isNaN(el);
}

function isFunction(el) {
    return typeof el === 'function';
}

function isHtmlElement(el) {
    return el !== undefined && el instanceof window.Element;
}

function isEnumerable(el) {
    return el && (typeof el === 'object' && el.length === 0 ?
        true : isFinite(el.length) && el[el.length - 1] !== undefined);
}

function isNull(el) {
    return el === null;
}

function isUndefined(el) {
    return el === undefined;
}
