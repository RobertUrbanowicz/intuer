module.exports = {
    select: select,
    selectUp: selectUp,
    selectShallow: selectShallow,
    selectUntil: selectUntil,
}

/**
 *  @param {string} selector id, class or attribute (with optional value, eg: type=text)
 *  @param {object} [context] DOM element to search in
 */
function select(selector, context){
    if ( ! selector ) throw 'Selector is required';
    let contextSupplied = context ? true : false;
    if (typeof context === 'string') {
        context = select(context);
        if (!context) return [];
        if (context.length !== undefined && context.length !== 0 ) context = context[0];
    }
    else if ( ! context ){
        context = window.document;
        contextSupplied = false;
    }

    const cfg = getTypeAndValue(selector);

    if (cfg.type === '#'){
        if (contextSupplied) return getElementsByAttribute('id=' + cfg.value, context);
        return [document.getElementById(cfg.value)];
    }
    else if (cfg.type === '.') return context.getElementsByClassName(cfg.value);
    else if (cfg.type === '[') return getElementsByAttribute(cfg.value, context);
    else return context.getElementsByTagName(selector);
}

function getTypeAndValue(selector){
    const fSign = selector.substr(0, 1);

    if (fSign === '#') return {type: '#', value: selector.substr(1)};
    else if (fSign === '.') return {type: '.', value: selector.substr(1)};
    else if (fSign === '[') return {type: '[', value: selector.substr(1, selector.length - 2)};
    else return ({type: 't', value: selector});
}

/**
 * @param {object} element DOM element to start search from
 * @param {string} selector selector: id, class or attribute (with optional value, eg: type=text)
 */
function selectUp(element, selector){
    if ( ! element || ! selector ) throw 'Element and selector are required';
    let result;
    const cfg = getMatcherAndValue(selector);
    goUp(element);

    function goUp(element){
        if ( ! element.parentElement ) return;
        
        if ( cfg.matcher(element.parentElement, cfg.value) ){
            result = element.parentElement;
        } else {
            goUp(element.parentElement, selector);
        }
    }

    return result ? result : null;
}

function getMatcherAndValue(selector){
    const fSign = selector.substr(0, 1);

    if (fSign === '#') return { matcher: matchId, value: selector.substr(1) };
    else if (fSign === '.') return { matcher: matchClass, value: selector.substr(1) };
    else if (fSign === '[') return { matcher: matchAttribute, value: selector.substr(1, selector.length - 2).split('=') };
    else return { matcher: matchTagName, value: selector };
}

function matchId(el, id){
    return el.id == id;
}
function matchClass(el, cssClass){
    return el.className.indexOf(cssClass) > -1;
}
function matchTagName(el, tagName){
    return el.tagName == tagName.toUpperCase();
}
function matchAttribute(el, attr){
    const elAttr = el.getAttribute(attr[0]);
    if ( ! elAttr ) return false;
    if ( attr[1] ) return elAttr == attr[1];
    else return true;
}

/**
 *  @param {string} attr attribute to find (with optional value, eg: type=text)
 *  @param {object} [context] DOM element to search in
 */
function getElementsByAttribute(attr, context){
    if ( ! attr ) throw new Error('selector is required');
    if (typeof context === 'string') {
        context = select(context);
        if (!context) return [];
        if (context.length !== undefined && context.length !== 0 ) context = context[0];
    }
    else if ( ! context ) context = window.document;

    const result = [];
    attr = attr.split('=');	

    goDown(context);

    function goDown(el){
        const children = el.children;
        if ( ! children ) return;

        for (let i = 0; i < children.length; i++) {
            const elAttr = children[i].getAttribute(attr[0]);

            if (checkAttrValue(elAttr) ){
                result.push(children[i]);
            }
            goDown(children[i]);
        }
        
    }

    function checkAttrValue(elementValue){
        return attr[1] === elementValue || (!attr[1] && elementValue !== null && elementValue !== undefined);
    }

    return result;
}

/* select with nesting */
function selectShallow(selector, context) {
    const attrAndVal = getAttributeAndValue(selector);
    return getElementsByAttributeWithShallow(attrAndVal.attribute, attrAndVal.value, context);
}

function getElementsByAttributeWithShallow(attr, attrVal, context) {
	if (!attr) throw new Error('selector is required');
	if (!context) context = window.document;

	attr = attr.trim();
	if (attrVal) attrVal = attrVal.trim();

	const result = [];

	goDown(context, 1);

	function goDown(el) {
		const children = el.children;
		if (!children) return;

		for (let i = 0; i < children.length; i++) {
			const value = children[i][attr] || children[i].getAttribute(attr);

			if (checkAttrValue(value)) {
				result.push(children[i]);
            } else {
                goDown(children[i]);
            }
		}

	}

	function checkAttrValue(elementValue) {
        return attrVal === elementValue || (!attrVal && elementValue !== null && elementValue !== undefined);
	}

	return result;
}

function getAttributeAndValue(selector) {
	const fSign = selector.substr(0, 1);

	if (fSign === '#') return { attribute: 'id', value: selector.substr(1) };
	else if (fSign === '.') return { attribute: 'className', value: selector.substr(1) };
	else if (fSign === '[') {
		const val = selector.substr(1, selector.length - 2).split('=');
		return { attribute: val[0], value: val[1] };
	}
	else return ({ attribute: 'tagName', value: selector.toUpperCase() });
}

/* select until */
/**
 * @param {string} selected 
 * @param {string | string[]} until 
 * @param {Element} context 
 */
function selectUntil(selected, until, context) {
    selected = getAttributeAndValue(selected);
    until = typeof until === 'string' ? [until] : until;
    until = until.map(selector => {
        return getAttributeAndValue(selector);
    });
    return getElementsByAttributeUntil(selected, until, context);
}

function getElementsByAttributeUntil(selected, until, context) {
	if (!selected || !until) throw new Error('Two arguments are required');
	if (!context) context = window.document;

	const result = [];

	goDown(context);

	function goDown(el) {
		const children = el.children;
		if (!children) return;

		for (let i = 0; i < children.length; i++) {
            const child = children[i];

            if (exclude(child))
                continue;
            
            const selectValue = child[selected.attribute] || child.getAttribute(selected.attribute);
			if (checkAttrValue(selectValue, selected.value)) {
				result.push(child);
            }

			goDown(child);
		}

    }
    
    function exclude(child) {
        return until.find(cfg => {
            const untilValue = child[cfg.attribute] || child.getAttribute(cfg.attribute);

            if (checkAttrValue(untilValue, cfg.value))
                return true;

        }) ? true : false;
    }

	function checkAttrValue(elementValue, desiredValue) {
        return desiredValue === elementValue || (!desiredValue && elementValue !== null && elementValue !== undefined);
	}

	return result;
}
