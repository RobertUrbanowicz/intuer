const isString = require('../identifier').isString;
const selectors = require('./selectors');

module.exports = {
    select: selectWrap,
    selectUp: selectors.selectUp,
};

const select = window.document.querySelectorAll ? select_querySelector : selectors.select;

function selectWrap(selector, context) {
    const config = getItemExtractionConfig(selector);
    const el = select(config.selector, context);
    return extractItem(config.index, el);
}

function select_querySelector(selector, context) {
    if (context) {
        if (context.querySelectorAll) return context.querySelectorAll(selector);
        else if (isString(context)) {
            context = window.document.querySelector(context);
            if (context) return context.querySelectorAll(selector);
            else return [];
        }
        else throw 'Invalid second argument';
    } else {
        return window.document.querySelectorAll(selector);
    }
}

function getItemExtractionConfig(selector) {
    return {
        selector: selector.replace(/\[\d+\]$/, ''),
        index: getItemExtractionIndex(selector),
    }
}

function getItemExtractionIndex(selector) {
    const match = selector.match(/\[(\d+)\]$/);
    return match ? match[1] : null;
}

function extractItem(index, el) {
    return index ? el[index] : el;
}
