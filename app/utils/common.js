var identifier = require('./identifier.js');
var isEmptyObject = require('./objectUtils').isEmpty;

module.exports = {
	each: each,
	find: find,
	filter: filter,
	map: map,
	replaceEmptyObjectsAndArrays: replaceEmptyObjectsAndArrays,
	forcePush: forcePush,
	contains: contains,
};

function each(list, fn) {
	if (typeof fn !== 'function') throw 'Second argument must be a function';

	if (identifier.isArray(list)) {
		for (var i = 0; i < list.length; i++) {
			fn(list[i], i);
		}
	} else {
		if (!identifier.isObject(list)) {
			throw 'First argument must be an object or an array.';
		}
		for (var propName in list) {
			if (list.hasOwnProperty(propName)) {
				fn(list[propName], propName);
			}
		}
	}
}

function find(list, fn) {
	if (typeof fn !== 'function') throw 'Second argument must be a function';

	if (identifier.isArray(list)) {
		for (var i = 0; i < list.length; i++) {
			if (fn(list[i], i))
				return list[i];
		}
	} else {
		if (!identifier.isObject(list)) {
			throw 'First argument must be an object or an array.';
		}
		for (var propName in list) {
			if (list.hasOwnProperty(propName)) {
				if (fn(list[propName], propName))
					return list[propName];
			}
		}
	}
}

function filter(list, fn) {
	if (typeof fn !== 'function') throw 'Second argument must be a function';
	var result = [];

	if (identifier.isEnumerable(list)) {
		for (var i = 0; i < list.length; i++) {
			if (fn(list[i], i))
				result.push(list[i]);
		}
	} else {
		if (!identifier.isObject(list)) {
			throw 'First argument must be an object or an array.';
		}
		for (var propName in list) {
			if (list.hasOwnProperty(propName)) {
				if (fn(list[propName], propName))
					result.push(list[propName]);
			}
		}
	}

	return result;
}

function replaceEmptyObjectsAndArrays(el, replacer) {
	if (!identifier.isArray(el) && !identifier.isObject(el)) {
		throw 'First argument must be an array or object. Got ' + el;
	}
	replacer = replacer || null;

	for (const prop in el) {
		if ((Identifier.isObject(el[prop]) && Object.prototype.hasOwnProperty.call(el, prop))) {
			replaceEmptyObjectsAndArrays(el[prop], replacer);
			if (isEmptyObject(el[prop])) {
				el[prop] = replacer;
			}
		} else if (Identifier.isArray(el[prop])) {
			replaceEmptyObjectsAndArrays(el[prop], replacer);
			if (el[prop].length === 0) {
				el[prop] = replacer;
			}
		}
	}
}

function forcePush(obj, prop, el) {
	if (!obj[prop]) obj[prop] = [];
	obj[prop].push(el);
}

function contains(parent, el) {
	if (identifier.isString(parent) || identifier.isArray(parent)) {
		return parent.indexOf(el) > - 1;
	} else {
		return find(parent, item => {
			return item === el;
		}) ? true : false;
	}
}

function map(list, fn = (item) => item, resultEl) {
	let result, push;
	const pushToObject = (value, key) => { result[key] = value };
	const pushToArray = (value) => { result.push(value) };

	if (resultEl) {
		if (!identifier.isObject(resultEl))
			throw `Third argument can be array or object, got ${typeof resultEl}`;

		result = resultEl;

		if (identifier.isArray(resultEl)) {
			push = pushToArray;
		} else {
			push = pushToObject;
		}
	} else {
		if (identifier.isArray(list)) {
			result = [];
			push = pushToArray;
		} else {
			result = {};
			push = pushToObject;
		}
	}

	each(list, (item, key) => {
		push(
			fn(item, key),
			key
		);
	});

	return result;
}
