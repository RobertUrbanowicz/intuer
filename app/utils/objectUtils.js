const identifier = require('./identifier');

module.exports = {
	copy: copy,
	getLength: getLength,
	isEmpty: isEmpty,
	toArray: toArray,
	toKeysArray: toKeysArray,
}

function copy(src, cb) {
	if (identifier.isFunction(cb)) return _copyWithCallback(src, cb);
	return _copy(src);
}

function _copy(src) {
	const result = identifier.isArray(src) ? [] : identifier.isObject(src) && !identifier.isNull(src) ? {} : false;

	if (!result) {
		return src;
	}

	for (const p in src) {
		if ((identifier.isObject(src[p]) && src.hasOwnProperty(p)) || identifier.isArray(src[p])) {
			result[p] = _copy(src[p]);
		} else {
			result[p] = src[p];
		}
	}

	return result;
}

function _copyWithCallback(src, cb) {
	if (!identifier.isFunction(cb)) throw 'Second argument must be a function. Got ' + cb;

	const result = identifier.isObject(src) ? {} : identifier.isArray(src) ? [] : false;

	if (!result) {
		return src;
	}

	for (const p in src) {
		if ((identifier.isObject(src[p]) && src.hasOwnProperty(p)) || identifier.isArray(src[p])) {
			result[p] = _copyWithCallback(src[p]);
		} else {
			result[p] = cb(p, src);
		}
	}

	return result;
}

function getLength(obj, countInherited) {
	if (!identifier.isObject(obj)) throw 'First argument must be an object. Got ' + obj;
	let length = 0;

	if (countInherited)
		for (const p in obj)
			length++;
	else
		for (const p in obj)
			if (obj.hasOwnProperty(p))
				length++;

	return length;
}

function isEmpty(obj, countInherited) {
	return getLength(obj, countInherited) === 0;
}

function toArray(obj, cb) {
	if (!identifier.isObject(obj)) throw 'Argument must be an object. Got ' + obj;
	if (!cb) cb = el => el;

	const result = [];

	for (const p in obj) {
		if (obj.hasOwnProperty(p))
			result.push(
				cb(obj[p], p)
			);
	}

	return result;
}

function toKeysArray(obj, cb) {
	if (!identifier.isObject(obj)) throw 'Argument must be an object. Got ' + obj;
	if (!cb) cb = el => el;

	const result = [];

	for (const p in obj) {
		if (obj.hasOwnProperty(p))
			result.push(
				cb(p, obj[p])
			);
	}

	return result;
}
