function wrap(cfg) {
    if (cfg) return xHttp(cfg);
}

wrap.get = get;
wrap.post = post;

module.exports = wrap;


function get(url, cb) {
    return xHttp({
        type: 'get',
        url: url,
        success: cb.success,
        error: cb.error
    });
}

function post(url, data, cb) {
    return xHttp({
        type: 'post',
        url: url,
        data: data,
        success: cb.success,
        error: cb.error
    });
}

/**
 * @param {object} config.headers Header name to value.
 */
function xHttp(config) {
    var xhttp = new XMLHttpRequest();

    if (config.withCredentials) xhttp.withCredentials = config.withCredentials;
    if (config.responseType) xhttp.responseType = config.responseType;
    if (config.timeout) xhttp.timeout = config.timeout;
    var isAsync = config.synchronous !== true;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            var respObj = {
                data: transformResponseData(xhttp.response, xhttp.getResponseHeader('Content-type')),
                status: xhttp.status,
                statusText: xhttp.statusText,
                responseURL: xhttp.responseURL,
                headers: function (name) { return xhttp.getResponseHeader(name) },
                config: config,
            }
            if (xhttp.status > 199 && xhttp.status < 400) config.success(respObj);
            else config.error(respObj);
        }
    };

    xhttp.open(config.type, config.url, isAsync);
    setHeaders(xhttp, config.headers);
    xhttp.send(config.data);
    return xhttp;
}

function setHeaders(xhttp, headers) {
    if (typeof headers !== 'object') return;
    for (var name in headers) {
        xhttp.setRequestHeader(name, headers[name]);
    }
}

function transformResponseData(data, contentType) {
    if (contentType && contentType.indexOf('json') > - 1) {
        try {
            return JSON.parse(data);
        } catch (err) {
            console.warn('Parsing response data to object failed.', { responseData: data, error: err });
            return data;
        }
    } else {
        return data;
    }
}
