var identifier = require('./identifier.js');

module.exports = {
    queue: queue,
};


module.exports.Event = class Event {
    constructor(name) {
        this.name = name;
        this.id = 0;
        this.listeners = {};
        this.triggered = false;
    }

    listen(fn, cfg) {
        if (typeof fn === 'function') {
            cfg = cfg || {};

            this.id ++;
            this.listeners[this.id] = {
                fn: fn,
                once: cfg.once,
            };

            return this.id;
        } else {
            console.error(fn + ' is not a function');
        }
    }

    once(fn) {
        return this.listen(fn, { once: true });
    }

    stopListening(id, silently) {
        if (this.listeners[id]) {
            delete this.listeners[id];
        } else if (!silently) {
            console.error('There is no listener with id', id);
        }
    }

    trigger() {
        for (const id in this.listeners) {
            this.listeners[id].fn(...arguments);

            if (this.listeners[id].once)
                delete this.listeners[id];
        }
        this.triggered = true;
    }

    removeAllListeners() {
        this.listeners = {};
    }
}

function queue(events, cb) {
    if (!identifier.isEnumerable(events)) throw 'First argument must be enumerable.';
    if (typeof cb !== 'function') throw cb + ' is not a function.';

    const length = events.length;
    const completed = [];

    for (let i = 0; i < length; i++) {
        addCompletedSetter(i);
    }

    function addCompletedSetter(i) {
        events[i].listen(() => {
            completed[i] = true;
            if (checkIfCompleted()) cb();
        });
    }

    function checkIfCompleted() {
        for (var i = 0; i < length; i++) {
            if (!completed[i]) return false;
        }
        return true;
    }
}
