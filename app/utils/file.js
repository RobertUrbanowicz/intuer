let fs, pathParser;

if (require) {
    fs = require('fs');
    pathParser = require('path');
}

const handleError = require('./errorHandling');
const ajax = require('./ajax');
const cache = require('../view/cache');

module.exports = {
    getFromFileSystem: getFromFileSystem,
    getFromUrl: getFromUrl,
    get: get,
};


function get(platform, path, cb) {
    if (platform === 'nodejs') getFromFileSystem(path, cb);
    else if (platform === 'browser') getFromUrl(path, cb);

    else throw 'Proper platform is not specified. Can not get template for url: ' + path;
}


function getFromFileSystem(path, cb) {
    path = pathParser.normalize(path);

    const cached = cache.get(path);

    if (cached)
        cb(cached);
    else
        fs.readFile(path, 'utf8', function (err, template) {
            if (err)
                console.error('Errror getting file ' + path, err);
            else {
                if (typeof cb === 'function') cb(template);
                cache.put(path, template);
            }
        });
}

function getFromUrl(path, cb) {
    const cached = cache.get(path);

    if (cached)
        cb(cached);
    else
        ajax.get(path, {
            success: function (response) {
                if (typeof cb === 'function') cb(response.data);
                cache.put(path, response.data);
            },
            error: function (response) {
                handleError('gettingDataFromServer', response.status);
            },
        });
}
