function getEvalTime(toCall, repeats){
    var startTime = Date.now();

    for (var i = 0; i < repeats; i++){
        toCall();
    }

    return Date.now() - startTime;
}

function compareEvalTime(first, second, repeats){
    var timeOfFirst = getEvalTime(first, repeats);
    var timeOfSecond = getEvalTime(second, repeats);

    return {
        first: timeOfFirst,
        second: timeOfSecond
    }
}


/*
function f1(){
    selUp(sel('.widget-container')[0], '[data-view-id=speeddial]');
}

function f2(){
    $('.widget-container').parents('[data-view-id=speeddial]');
}

compareEvalTime(f1, f2, 1000);
*/