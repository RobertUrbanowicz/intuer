var errorList = {
    'gettingDataFromServer': 'Error getting data from server. Response status:',
}


module.exports = function(type, data){
    switch (type){
        case 'gettingDataFromServer':
            console.error(errorList.gettingDataFromServer, data);
            break;
    }
}