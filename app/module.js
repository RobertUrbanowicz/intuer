const identifier = require('./utils/identifier');
const event = require('./utils/event');
const select = require('./utils/view/selectWrap').select;
const Component = require('./component');
const { destroyComponents } = require('./component/instance');
const { Storage } = require('./model/storage');
const { createEvents } = require('./component/helpers');

const moduleHostElAttr = 'module';

/**
 * @param {String} name module name
 * @param {Object} config
 * @param {any} config.runIn Element to run module in. Css selector or DOM element
 */

class Module {
    constructor(name, config = {}) {
        const storage = new Storage();
        this.config = config;
        this.app = null;
        this.name = name;
        this.component = new Component(this);
        this.data = storage.data;
        this.onDataChange = storage.onchange.bind(storage);
        this.onBeforeInit = new event.Event();
        this.onInit = new event.Event();
        this.events = createEvents(config.events);
    }

    init() {
        this.el = getModuleHostEl(this.config.runIn, this.name);
        this.onBeforeInit.trigger();
        this.component.init();
        this.onInit.trigger();
    }

    destroy() {
        if (this.el)
            destroyComponents(this.el);
    }
}

function getModuleHostEl(runIn, name) {
    if (identifier.isString(runIn)) {
        return select(runIn);
    }

    if (identifier.isHtmlElement(runIn)) {
        return runIn;
    }

    const moduleElSelector = `[${moduleHostElAttr}=${name}]`;
    const hostEl = select(moduleElSelector)[0];

    if (identifier.isHtmlElement(hostEl)) {
        return hostEl;
    }

    throw new Error(`Can not find host element for module "${name}"`);
}

module.exports = Module;
