const { select } = require('../utils/view/selectWrap');
const { each } = require('../utils/common');
const id = require('../utils/identifier');

module.exports = fill;

function fill(data, el){
    if ( ! id.isObject(data) ) throw 'Data must be an object.';

    each(data, (value, selector) => {
        const elements = select(selector, el);
        insert(value, elements);
    });
}

function insert(value, elements){
    if ( ! elements ) return;
    if ( ! id.isEnumerable(elements) ) elements = [elements];

    each(elements, function(el){
        el.innerHTML = value;
    });
}
