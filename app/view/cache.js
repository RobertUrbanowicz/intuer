module.exports = {
    put: put,
    get: get,
};

var cache = {};

function put(path, template) {
    cache[path] = template;
}

function get(path) {
    return cache[path];
}
