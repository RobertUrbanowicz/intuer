const { selectUntil } = require('../utils/view/selectors');
const { each, map } = require('../utils/common');
const { isFunction } = require('../utils/identifier');
const Input = require('../helpers/input');
const { getModelPath, findKey } = require('./helpers');

const eventsBindingAttr = 'events';
const repeatAttr = '[repeat]';

module.exports = bindEventsWithMethods;


function bindEventsWithMethods(view, cpnInstance, repeatScopes) {
    const inputs = [];

    each(mapEventsWithMethods(view), (elementAndEvents) => {
        
        each(elementAndEvents.events, (evObj) => {
            if (!isFunction(cpnInstance[evObj.method])) {
                throw new Error(`There is no "${evObj.method}" method on "${cpnInstance.fullName}" component`);
            }

            const params = handleParams(evObj.params, cpnInstance.__storage, repeatScopes);
            const input = new Input(evObj.eventName);
            handleModifiers(input, evObj.modifiers);
            
            input
                .on(elementAndEvents.element)
                .listen(callMethod);

            inputs.push(input);
    
            function callMethod(e) {
                cpnInstance[evObj.method].call(cpnInstance, e, elementAndEvents.element, ...params);
            }
        });

    });
    
    return inputs;
}

function handleModifiers(input, modifiers) {
    modifiers.forEach(modifier => {
        if (modifier === 'withShift') {
            input.withShift();
        }
        else if (modifier === 'withCtrl') {
            input.withCtrl();
        }
        else if (modifier === 'withAlt') {
            input.withAlt();
        }
        else if (modifier === 'withMeta') {
            input.withMeta();
        }
        else if (modifier.match(/^\d+$/)) {  // keyCode
            const keyCodeMatch = modifier.match(/^\d+$/);
            const keyCode = Number(keyCodeMatch[0]);
            input.byKeyCode(keyCode);
        }
        else if (modifier.match(/^[a-zA-Z]+$/)) {  // keyName
            const keyNameMatch = modifier.match(/^[a-zA-Z]+$/);
            const keyName = keyNameMatch[0];
            input.byKey(keyName);
        }
    });
}

function handleParams(params, storage, repeatScopes) {
    return map(params, param => {
        const paramConfig = parseParamForType(param);

        if (!paramConfig) {
            throw `Invalid param in method at: ${param} in ${params}`;
        }

        switch (paramConfig.type) {
            case 'string':
            case 'number':
            case 'boolean':
                return paramConfig.value;

            case 'variable': {
                const modelPath = repeatScopes ? getModelPath(repeatScopes, paramConfig.value) : paramConfig.value;
                let value = storage.get(modelPath);
                value = value === undefined ? findKey(paramConfig.value, repeatScopes) || undefined : value;
        
                return value;
            }
        }
    });
}

function parseParamForType(param) {
    if (param.startsWith(`'`) || param.startsWith(`"`))
        return { type: 'string', value: param.replace(/['"]/g, '') };

    if (param.startsWith(`:`))
        return { type: 'variable', value: param.replace(/:/g, '') };

    if (param.match(/^-?\d+(\.\d+)?$/))
        return { type: 'number', value: parseFloat(param) };

    if (param === 'true')
        return { type: 'boolean', value: true };

    if (param === 'false')
        return { type: 'boolean', value: false };

    return null;
}

function mapEventsWithMethods(view) {
    const map = [];
    const eventsBindingSelector = '[' + eventsBindingAttr + ']';
    const elements = selectUntil(eventsBindingSelector, repeatAttr, view);
    if (view.getAttribute(eventsBindingAttr))
        elements.push(view);

    for (let i = 0; i < elements.length; i++) {
        mapElement(elements[i]);
    }

    function mapElement(element) {
        const result = {
            element: element,
            events: [],
        };

        const events = element
            .getAttribute(eventsBindingAttr)
            .replace(/[\s\r\n]/g, '')
            .replace(/;+$/, '')
            .split(';');

        each(events, addEvent);

        function addEvent(evAndMethodStr) {
            const evAndMethod = evAndMethodStr.split('='); // ['keyup.enter.withShift', 'doSomething(param1, param2)']
            if (!evAndMethod[1]) handleAttrValError(evAndMethodStr, element, 'Method is missing.');

            const paramsReg = /\(([a-zA-Z0-9-_,'":.]+)?\)/;
            const paramsMatch = evAndMethod[1].match(paramsReg);

            const method = evAndMethod[1].replace(paramsReg, '');
            const eventName = evAndMethod[0].split('.')[0]; // 'keyup'

            let modifiers = [];
            let params = [];

            if (evAndMethod[0].match(`${eventName}.`)) {
                const modifiersStr = evAndMethod[0].replace(`${eventName}.`, ''); // 'enter.withShift' || ''
                modifiers = modifiersStr.split('.');
            }

            if (paramsMatch && paramsMatch[1]) {
                params = paramsMatch[1].split(',');
            }

            result.events.push({
                eventName: eventName,
                modifiers: modifiers,
                method: method,
                params: params,
            });
        }

        map.push(result);
    }

    return map;
}

function handleAttrValError(value, element, msg) {
    const type = `Invalid value of event binding property at: ${value}`;
        console.error({
            type: type,
            msg: msg,
            element: element,
        });
        throw type;
}
