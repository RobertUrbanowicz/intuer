const { each } = require('../../utils/common');
const copy = require('../../utils/objectUtils').copy;
const { selectShallow } = require('../../utils/view/selectors');
const bindModel = require('../model');
const bindEventsWithMethods = require('../events');
const id = require('../../utils/identifier');
const { isValidPath, validateIteratee } = require('./helpers');
const { getModelPath } = require('../helpers');

module.exports = applyRepeaters;

const _mainVariableAttr = 'repeat';
const _variableAttr = 'var';
const _keyAttr = 'key';

let uniqueId = 0;


function applyRepeaters(mainEl, storage, scopes, viewServicesConfig, instance) {
    const repeaters = selectShallow('[' + _mainVariableAttr + ']', mainEl);

    each(repeaters, function(el) {
        applyRepeater(el, storage, scopes, viewServicesConfig, instance);
    });
}

function applyRepeater(mainEl, storage, scopes, viewServicesConfig, instance) {
    const mainVarName = mainEl.getAttribute(_mainVariableAttr);
    const mainVarModelPath = scopes ? getModelPath(scopes, mainVarName) : mainVarName;

    const iterVarName = mainEl.getAttribute(_variableAttr);
    const iteratee = storage.get(mainVarModelPath);

    validateIteratee(iteratee, mainVarModelPath);

    const repeatId = ++uniqueId;

    mainEl.style.setProperty('display', 'none', 'important');
    mainEl.setAttribute('data-repeat-id', repeatId);

    repeat(iteratee, mainVarModelPath);
    storage.onchange(mainVarModelPath, repeat);

    function repeat(iteratee, actualChangePath) {
        const isArray = id.isArray(iteratee);

        if (!isValidPath(mainVarModelPath, actualChangePath, isArray))
            return;

        if (!id.isObject(iteratee))
            return;

        removeIterationElements(mainEl, mainVarName, instance.__modelListeners);
        removeEvents(instance.__repeatEvents, repeatId);

        let lastCopy = mainEl;

        each(iteratee, function (iterVal, key) {
            // on removing elements from array, 'length' prop is not updated yet
            if (isArray && iterVal === undefined)
                return;

            const iterEl = mainEl.cloneNode(true);
            iterEl.style.setProperty('display', '');
            
            const keyVar = iterEl.getAttribute(_keyAttr);
            const iterModelPath = mainVarModelPath + '.' + key;
            const newScopes = scopes ? copy(scopes) : [];
            
            const scope = {
                variable: iterVarName, // alias for model path
                modelPath: iterModelPath, // storage path
                key: key, // key value
                keyVar: keyVar, // variable name, that holds key value; 
            };
    
            newScopes.push(scope);

            const modelConfig = bindModel(iterEl, storage, newScopes, viewServicesConfig.list);
            instance.__modelListeners.assignToHostElement(iterEl, modelConfig.listeners);

            applyRepeaters(iterEl, storage, newScopes, viewServicesConfig);
            viewServicesConfig.runAll(iterEl, modelConfig.viewServices);

            lastCopy.insertAdjacentElement('afterend', iterEl);
            lastCopy = iterEl;

            instance.__repeatEvents[repeatId].push(bindEventsWithMethods(iterEl, instance, newScopes));
        });
    }

}


function removeIterationElements(previousEl, attrVal, modelListeners) {
    const elementsToRemove = [];

    add(previousEl);

    function add(prev) {
        if (
            prev.nextElementSibling &&
            prev.nextElementSibling.getAttribute(_mainVariableAttr) === attrVal
        ) {
            elementsToRemove.push(prev.nextElementSibling);
            add(prev.nextElementSibling);
        }
    }

    elementsToRemove.forEach((el) => {
        modelListeners.removeFromHostElement(el);
        el.parentElement.removeChild(el);
    });
}

function removeEvents(instanceRepeatEvents, repeatId) {
    if (instanceRepeatEvents[repeatId])
        each(instanceRepeatEvents[repeatId], (array) => {
            array.forEach(input => input.stopListening());
        });
    else
        instanceRepeatEvents[repeatId] = [];
}
