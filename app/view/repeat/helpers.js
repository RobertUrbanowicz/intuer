
const id = require('../../utils/identifier');

module.exports = {
    isValidPath: isValidPath,
    validateIteratee: validateIteratee,
};


function isValidPath(mainPath, changePath, isIterateeArray) {
    const mainPathSegments = mainPath.split('.');
    const changePathSegments = changePath.split('.');

    // change of element that is nested more than one level in element that we listen to
    if (changePathSegments.length > mainPathSegments.length + 1)
        return false;

    // don't rerender on array's length change
    if (isIterateeArray && changePathSegments[changePathSegments.length -1] === 'length')
        return false;

    return true;
}


function validateIteratee(iteratee, mainVarModelPath) {
    if (!id.isObject(iteratee) && !id.isUndefined(iteratee))
        throw new Error(`Only objects and arrays can be iterated! "${mainVarModelPath}" is ${typeof iteratee}.`);
}
