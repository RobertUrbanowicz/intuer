const { each } = require('../utils/common');
const { selectUntil } = require('../utils/view/selectors');

const modelBindingAttrSelector = '[bind]';
const repeatAttrSelector = '[repeat]';

class ViewService {
    constructor() {
        this.services = {};
    }

    add(name, fn) {
        this.services[name] = fn;
    }

    runAll(element, storage, bound) {
        const modelListeners = [];
        modelListeners.concat(
            this.runBound(storage, bound),
            this.runNotBound(element, storage)
        );
        return modelListeners;
    }

    /**
     * @description Run view services on elements that have data bindings
    */
    runBound(storage, bound) {
        const modelListeners = [];

        each(bound, binding => {
            const service = this.services[binding.serviceName];

            service({
                storage: storage,
                model: storage.data,
                el: binding.element,
                boundValues: binding.attributes,
                onModelChange: (path, cb) => {
                    const listener = storage.onchange(path, cb);
                    modelListeners.push(listener);
                },
                onBoundChange: (attr, fn) => {
                    if (!binding.attributesListeners[attr])
                        throw {
                            errorType: `viewService "${binding.serviceName}" onBoundChange error`,
                            error: `Attribute "${attr}" has not binding with model in this element`,
                            element: binding.element,
                        };
 
                    const listener = binding.attributesListeners[attr](fn);
                    modelListeners.push(listener);
                },
                modelPaths: binding.modelPaths,
            });

        });

        return modelListeners;
    }
    /**
     * @description Run view services on elements that don't have data bindings
    */
    runNotBound(element, storage) {
        const modelListeners = [];

        each(this.services, (service, name) => {
            const attr = `[${name}]`;
            const hosts = selectUntil(attr, [modelBindingAttrSelector, repeatAttrSelector], element);

            each(hosts, el => {
                service({
                    storage: storage,
                    model: storage.data,
                    el: el,
                    onModelChange: (path, cb) => {
                        const listener = storage.onchange(path, cb);
                        modelListeners.push(listener);
                    },
                    onBoundChange: () => {
                        console.error('This element has not any binded attributes', element);
                    },
                });
            });

        });

        return modelListeners;
    }
}

module.exports = ViewService;
