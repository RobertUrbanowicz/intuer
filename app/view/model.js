const { Storage } = require('../model/storage');
const selectUntil = require('../utils/view/selectors').selectUntil;
const identifier = require('../utils/identifier');
const { each } = require('../utils/common');
const Input = require('../helpers/input');
const { getModelPath, findKey } = require('./helpers');

module.exports = createModel;

const modelBindingAttr = 'bind';
const repeatAttr = 'repeat';
const twoWayBinded = ['SELECT', 'INPUT', 'TEXTAREA'];
const directProperties = ['innerHTML', 'innerText', 'textContent', 'id', 'style', 'value', 'checked', 'disabled', 'readonly'];

function getEventNameByInputType(type) {
    return type === 'checkbox' ? 'click' : 'input';
}

function getValueAttrByInputType(type) {
    return type === 'checkbox' ? 'checked' : 'value';
}

function createModel(view, storage = new Storage(), scopes = [], viewServicesList) {
    const mainConfig = {
        listeners: {
            viewListeners: [],
            storageListeners: [],
        },
        viewServices: [],
    };
    
    mapModelWithElements();

    return mainConfig;


    function mapModelWithElements() {
        const elements = selectUntil('[' + modelBindingAttr + ']', '[' + repeatAttr + ']', view);

        // add root el
        if (view.getAttribute(modelBindingAttr)) {
            elements.push(view);
        }

        for (let i = 0; i < elements.length; i++) {
            mapElement(elements[i]);
        }

        function mapElement(element) {
            const bindings = element
                .getAttribute(modelBindingAttr)
                .replace(/\s+/g, '')
                .replace(/,/g, ';')
                .replace(/;+/g, ';')
                .split(';');

            const attributes = [];

            each(bindings, binding => {
                const attr = getAttributeConfig(binding, element);
                attributes.push(attr);
            });

            bindModelToView(element, attributes);
        }
    }

    function getAttributeConfig(binding, element) {
        binding = binding.split('=');
        handleAttrValError(binding, element);

        const elementAttribute = binding[0];
        const variableName = binding[1];

        const modelPath = getModelPath(scopes, variableName);

        const config = {
            name: elementAttribute,
            modelPath: modelPath,
            preventViewUpdate: false,
        };
        
        if (shouldBindTwoWay(element.tagName, elementAttribute))
            bindViewToModel(element, config);

        return config;
    }

    function handleAttrValError(attr, element) {
        if (!attr[1]) {
            console.error({
                error: 'Invalid value of model binding property at: ' + attr[0],
                element: element,
                propertyValue: element.getAttribute(modelBindingAttr),
            });
            throw 'Invalid value of model binding property at: ' + attr[0];
        }
    }

    function shouldBindTwoWay(tagName, attrName) {
        return (attrName === 'value' || attrName === 'checked') && twoWayBinded.includes(tagName);
    }


    /* view to model binding */
    function bindViewToModel(el, config) {
        const eventName = getEventNameByNodeName(el.nodeName, el.type);
        if (!eventName) return;

        const valueAttr = getValueAttrByInputType(el.type);
        const input = new Input(eventName);

        input
            .on(el)
            .listen(setValue);

        mainConfig.listeners.viewListeners.push(input);
        
        function setValue() {
            config.preventViewUpdate = true;
            storage.set(config.modelPath, el[valueAttr], true);
            config.preventViewUpdate = false;
        }
    }
        
    function getEventNameByNodeName(nodeName, type) {
        switch (nodeName) {
            case 'SELECT': return 'change';
            case 'INPUT':
                return getEventNameByInputType(type);
            case 'TEXTAREA': return 'input';
        }
    }


    /* model to view binding */
    function bindModelToView(element, attributes) {

        each(attributes, attr => {
            const presentValue = storage.get(attr.modelPath);
            
            // set values already present in model
            if (!identifier.isUndefined(presentValue)) {
                updateElement(element, attr.name, presentValue);
            }
            
            // set value of key in repeat
            const key = findKey(attr.modelPath, scopes);
            if (!identifier.isNull(key)) {
                updateElement(element, attr.name, key);
            }

            mainConfig.listeners.storageListeners.push(
                storage.onchange(attr.modelPath, (newValue) => {
                    if (!attr.preventViewUpdate)
                        updateElement(element, attr.name, newValue);
                })
            );

        });
            
        // viewServices

        each(viewServicesList, name => {
            if (!element.hasAttribute(name)) return;

            const bound = {
                serviceName: name,
                element: element,
                attributes: {},
                attributesListeners: {},
                modelPaths: {},
            };

            each(attributes, attr => {
                bound.attributesListeners[attr.name] = fn => {
                    return storage.onchange(attr.modelPath, fn);
                };
                bound.attributes[attr.name] = storage.get(attr.modelPath);
                bound.modelPaths[attr.name] = attr.modelPath;
            });
            
            mainConfig.viewServices.push(bound);
        });
    }
    
    function updateElement(el, attr, newValue) {
        if (directProperties.includes(attr))
            el[attr] = newValue;
        else
            el.setAttribute(attr, newValue);
    }

}
