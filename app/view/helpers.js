const { find } = require('../utils/common');
const { pathsMatch } = require('../model/storage');

module.exports = {
  findKey: findKey,
  getModelPath: getModelPath,
};


function findKey(varName, scopes) {
  const scope = find(scopes, scope => {
      return scope.keyVar === varName;
  });
  return scope ? scope.key : null;
}

// repeat=one var=var1
// repeat=var1.two var=var2 repeatModelPath: one.(key).two
// repeat=var2.three.four repeatModelPath: one.(key).two.(key).three.four
// scopes.variable=[var1, var2]
// scope.modelPath=one.(key).two(key) varName=var2.three.four      one.(key).two(key)|.three.four
// one = [
//  0: { one.0
//    two: [ one.0.two
//      0: { one.0.two.0
//        three: [ one.0.two.0.three
//          
//        ] 
//      }
//    ]
//  },
//  1: {},
// ]
function getModelPath(scopes, varName) {
  let path = varName;

  const scope = find(scopes, (scope) => {
      return pathsMatch(scope.variable, varName);
  });

  if (scope) {
      const varNameReg = new RegExp(`${scope.variable}(\.)?`);
      const suffix = varName.replace(varNameReg, '\$1');
      path = `${scope.modelPath}${suffix}`;
  }

  return path;
}
