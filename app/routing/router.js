const Input = require('../helpers/input');
const State = require('./state');

module.exports = class Router {
    constructor() {
        this.hashChangeInput = new Input('hashchange')
            .on(window)
            .listen(this.onHashChange.bind(this));

        /**
         * @type State[]
         */
        this.connectedStates = [];
        /**
         * @type State
         */
        this.activeState = null;
    }

    onHashChange() {
        const hash = window.location.hash.split('#')[1];
        const state = this.findStateForHash(hash);
        // todo
        // if (state) {
        //     if (!this.activeState || this.activeState.name !== state.name) {

        //         this.activeState
        //     }
        // }
    }

    findStateForHash(hash) {
        return this.connectedStates.find(state => state.url === hash);
    }

    destroy() {
        this.hashChangeInput.stopListening();
    }

    /**
     * @param {State} state 
     */
    connectState(state) {
        this.connectedStates.push(state);
    }
}
