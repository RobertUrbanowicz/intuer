const id = require('../utils/identifier');

module.exports = class StateBase {
    constructor(name) {
        this.app = null;
        this.name = name;
        this.parentState = null;
        this.childStates = [];
        this.connectedModules = [];
        this.connectedComponents = [];
        this.depsLoaded = false;
    }

/**
 * @param {String | Array} modules
 */
    connectModules(modules) {
        modules = id.isArray(modules) ? modules : [modules];
        modules.forEach(mod => this.connectedModules.push(mod));
        return this;
    }
    
/**
 * @param {String | Array} paths
 */
    connectComponents(paths) {
        paths = id.isArray(paths) ? paths : [paths];
        paths.forEach(path => this.connectedComponents.push(path));
        return this;
    }
    
    addChildState(state) {
        state.parentState = this;
        this.childStates[state.name] = state;
        return this;
    }

    init() {
        if (!this.depsLoaded) this.loadDependencies();
        this.connectedModules.forEach(mod => mod.init());
        this.connectedComponents.forEach(cpn => cpn.init());
        this.childStates.forEach(state => state.init());
    }

    destroy() {
        this.connectedModules.forEach(mod => mod.destroy());
        this.connectedComponents.forEach(cpn => cpn.destroy());
        this.childStates.forEach(state => state.destroy());
    }

    loadDependencies() {
        this.connectedModules.forEach((name, index) => {
            this.connectedModules[index] = this.getModule(name);
        });

        this.connectedComponents.forEach((path, index) => {
            this.connectedComponents[index] = this.getComponentDefinition(path);
        });

        this.depsLoaded = true;
    }

    getModule(name) {
        const module = this.app.modules[name];
        if (!module) throw new Error(`Can not find module "${name}" defined in state "${this.name}".`);
        return module;
    }

    getComponentDefinition(path) {
        path = path.split('.');
        const moduleName = path[0];
        const componentName = path[1];

        const module = this.app.modules[moduleName];
        if (!module) throw new Error(`Invalid component path "${path}". Can not find module "${moduleName}" defined in state "${this.name}".`);
        
        const definition = module.component.getDefinition(componentName);
        if (!definition) throw new Error(`Invalid component path "${path}". Can not find component "${componentName}" defined in state "${this.name}".`);

        return definition;
    }
}
