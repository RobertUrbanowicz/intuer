const identifier = require('../utils/identifier');
const { each, contains } = require('../utils/common');
const { Event } = require('../utils/event');
const createProxy = require('./proxy');

module.exports = {
    Storage: Storage,
    pathsMatch: pathsMatch,
};


function pathsMatch(changePath, mainPath) {
    const mainPathSegments = mainPath.split('.');
    const changePathSegments = changePath.split('.');

    const lastSegmentToCheck = changePathSegments.length >= mainPathSegments.length ? mainPathSegments.length : changePathSegments.length;

    // Compare segments of paths to the last common segment.
    // All segments of actual change path must be the same as in main path that we listen to.
    for (let i = 0; i < lastSegmentToCheck; i++) {
        if (changePathSegments[i] !== mainPathSegments[i])
            return false;
    }
    return true;
}


function Storage() {
    const _this = this;

    const events = {};

    this.data = createProxy(this, {});
    this.get = get;
    this.set = set;
    this.remove = remove;
    this.has = has;
    this.onchange = onchange;
    this.removeAllListeners = removeAllListeners;
    this.triggerChange = _triggerEvents;

    /**
     * @param {String} path object path, eg.: 'cars.fastest.color'
     * @param {Boolean} returnParent 
     */
    function get(path, returnParent) {
        if (!path) return _this.data;
        if (!identifier.isString(path)) throw 'Argument must be a string or empty. Got ' + path;

        path = path.split('.');

        const targetDepth = path.length -1;
        let currentDepth = -1;

        return goDown(_this.data);

        function goDown(obj) {
            currentDepth ++;

            const currPathSegment = path[currentDepth];
            const hasAttribute = ! identifier.isUndefined(obj[currPathSegment]);

            if (hasAttribute) {
                if (currentDepth === targetDepth)
                    return returnParent ? obj : obj[currPathSegment];
                return goDown(obj[currPathSegment]);
            } else if (currentDepth === targetDepth && returnParent) {
                return obj;
            } else {
                return undefined;
            }
        }
    }

    function remove(paths, omitEvents) {
        if (identifier.isString(paths)) {
            return changeValue(paths, function (parent, propName) {
                delete parent[propName];
            }, omitEvents);
        }
        else if (identifier.isArray(paths)) {
            each(paths, function (path) {
                changeValue(path, function (parent, propName) {
                    delete parent[propName];
                }, omitEvents);
            });
            return _this;
        }
        throw 'Invalid first argument';
    }

    function set(mapOrPath, value, omitEvents) {
        if (identifier.isString(mapOrPath)) {
            return changeValue(mapOrPath, (parent, propName) => {
                parent[propName] = value;
            }, omitEvents);
        }
        else if (identifier.isObject(mapOrPath)) {
            each(mapOrPath, (value, path) => {
                changeValue(path, (parent, propName) => {
                    parent[propName] = value;
                }, omitEvents);
            });
            return _this;
        }
        throw 'Invalid first argument';
    }

    function changeValue(path, changer, omitEvents) {
        const pathSegments = path.split('.');
        const propName = pathSegments[pathSegments.length - 1];
        const parent = get(path, true);

        if (parent) {
            if (omitEvents) {
                changer(parent, propName);
            } else {
                const preparedEvents = prepareEvents(path, parent[propName]);
                changer(parent, propName);
                triggerEvents(preparedEvents, path);
            }
        } else {
            console.error('Invalid path (' + path + ') for value', value);
        }
        return _this;
    }

    function has(path) {
        return get(path) !== undefined;
    }

    function onchange(one, two, three) {
        let path, fn, cfg;
        validateArgs();

        if (!events[path]) events[path] = new Event(path);
        const listenerId = events[path].listen.call(events[path], fn, cfg);

        return {
            id: listenerId,
            path: path,
            stopListening: (silently) => {
                events[path].stopListening.call(events[path], listenerId, silently);
            },
        };

        function validateArgs() {
            if (identifier.isString(one)) {
                path = one;
                if (identifier.isFunction(two)) fn = two;
                else throw 'Second argument must be a function. Got ' + two;
                cfg = three;
            } else {
                if (identifier.isFunction(one)) {
                    path = '';
                    fn = one;
                    cfg = two;
                } else
                    throw 'First argument must be a string or a function. Got ' + one;
            }
        }
    }

    function removeAllListeners() {
        each(events, event => {
           event.removeAllListeners(); 
        });
    }

    function prepareEvents(pathOfChange, oldVal) {
        const result = [];

        each(events, (event, evPath) => {
            if (pathsMatch(pathOfChange, evPath))
                result.push({
                    event: event,
                    path: evPath,
                });
        });

        return result;
    }

    function triggerEvents(arr, pathOfChange) {
        if (events['']) {
            events[''].trigger.call(events[''], get(pathOfChange), pathOfChange);
        }

        each(arr, cfg => {
            const newValue = get(cfg.path);
            cfg.event.trigger.call(cfg.event, newValue, pathOfChange);
        });
    }

    function _triggerEvents(pathOfChange) {
        pathOfChange = pathOfChange || '';

        if (events[''])
            events[''].trigger.call(events[''], get(pathOfChange), pathOfChange);

        each(events, (e, evPath) => {
            if (pathsMatch(pathOfChange, evPath))
                e.trigger.call(e, get(evPath), pathOfChange);
        });
    }

}
