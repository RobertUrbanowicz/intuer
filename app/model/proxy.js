const { isArray } = require('../utils/identifier');

module.exports = setProxy = (storage, obj = {}, pathPrefix) => {

    Object.keys(obj).forEach(prop => {
        if (obj[prop] && typeof obj[prop] === 'object') {
            const path = pathPrefix ? `${pathPrefix}.${prop}` : prop;
            obj[prop] = setProxy(storage, obj[prop], path);
        }
    });

    return new Proxy(obj, getHandler(storage, pathPrefix));
};

function getHandler(storage, pathPrefix) {
    return {
        set: (obj, prop, value) => {
            console.log(`${prop} =>`, value);

            const path = pathPrefix ? `${pathPrefix}.${prop}` : prop;
            
            if (typeof value === 'object') {
                obj[prop] = setProxy(storage, value, path);
            } else {
                obj[prop] = value;
            }

            storage.triggerChange(path);
    
            return true;
        },
    
        deleteProperty: (obj, prop) => {
            delete obj[prop];
    
            console.log(`"${prop}" removed`);

            const path = pathPrefix ? `${pathPrefix}.${prop}` : prop;
            storage.triggerChange(path);
    
            return true;
        }
    };
}
