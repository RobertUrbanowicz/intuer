const { Event } = require('./utils/event');
const StateBase = require('./routing/state');
const ViewService = require('./view/viewService');
const Module = require('./module');

module.exports = class App {
    constructor(config) {
        this.config = config;
        this.validateConfig();

        const app = this;
        this.viewService = new ViewService();
        
        /**
         * @type Map<string, Module>
         */
        this.modules = new Map();
        this.onInit = new Event();
        this.onDestroy = new Event();

        this.State = class State extends StateBase {
            constructor(name) {
                super(name);
                this.app = app;
            }
        }
    }

    validateConfig() {
        if (this.config.platform !== 'nodejs' && this.config.platform !== 'browser')
            throw new Error('You have to specify proper application platform (browser or nodejs).');
    }

    init() {
        this.modules.forEach(module => module.init());
        this.onInit.trigger();
    }

    addModule(module) {
        module.app = this;
        this.modules.set(module.name, module);
    }

    destroy() {
        this.modules.forEach(module => module.destroy());
        this.onDestroy.trigger();
    }
};
