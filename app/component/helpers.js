const event = require('../utils/event');
const identifier = require('../utils/identifier');
const { each, find } = require('../utils/common');
const select = require('../utils/view/selectWrap').select;

const hostElCpnNameAttribute = 'data-component-name';
const styleElCpnNameAttribute = 'data-for-component';

module.exports = {
    getUiElements: getUiElements,
    createEvents: createEvents,
    getPublicInstances: getPublicInstances,
    createLifecycleEvents: createLifecycleEvents,
    removeAllListeners: removeAllListeners,
    appendStyle: appendStyle,
    wrapElement: wrapElement,
    parseHTML: parseHTML,
    hasRootElement: hasRootElement,
    failsafeCall: failsafeCall,
};

function getUiElements(selectors, view) {
    const result = {};

    for (const sel in selectors) {
        let el = select(selectors[sel], view);

        if (el && isIdSelector(selectors[sel])) {
            el = el[0];
        }

        result[sel] = el;
    }

    return result;
}

function isIdSelector(selector) {
    return selector.match(/#\w+$/) ? true : false;
}

function createEvents(eventNames) {
    var events = {};
    if (!identifier.isArray(eventNames)) return events;
    for (var i = 0; i < eventNames.length; i++) {
        events[eventNames[i]] = new event.Event(eventNames[i]);
    }
    return events;
}

function getPublicInstances(list, componentHub) {
    if (!identifier.isObject(list)) return null;
    const dependingComponents = {};

    each(list, (path, name) => {
        const moduleAndCpnNames = getmoduleAndCpnNames(path);
        const depCpn = getInstance(moduleAndCpnNames, componentHub);

        if (depCpn) {
            dependingComponents[name] = depCpn;
        } else {
            console.warn('Could not find component with name ' + name);
        }
    });

    return dependingComponents;
}

function getInstance(moduleAndCpnNames, componentHub) {
    if (moduleAndCpnNames.module) {
        var module = componentHub.parentModule.app.modules.get(moduleAndCpnNames.module);
        if (module) return module.component.getPublicInstance(moduleAndCpnNames.cpn);
    } else {
        return componentHub.getPublicInstance(moduleAndCpnNames.cpn);
    }
}

function getmoduleAndCpnNames(name) {
    var result = {
        cpn: name,
    };
    for (var i = 0; i < name.length; i++) {
        if (name[i] === '.') {
            result.module = name.substring(0, i);
            result.cpn = name.substring(i + 1);
            break;
        }
    }
    return result;
}

function createLifecycleEvents() {
    return {
        init: new event.Event(),
        beforeAttach: new event.Event(),
        attach: new event.Event(),
        ready: new event.Event(),
        detach: new event.Event(),
        destroy: new event.Event(),
    };
}

function removeAllListeners(events) {
    each(events, function (event) {
        event.removeAllListeners();
    });
}

function appendStyle(cpnName, style) {
    if (!style) return;

    const head = window.document.getElementsByTagName('head')[0];
    const styleElements = head.getElementsByTagName('style');

    !find(styleElements, el => {
        if (el.getAttribute(styleElCpnNameAttribute) === cpnName)
            return true;
    }) ? createStyleEl() : void(0);

    function createStyleEl() {
        const styleEl = window.document.createElement('style');
        styleEl.setAttribute(styleElCpnNameAttribute, cpnName);
        styleEl.textContent = parseStyle();
        head.appendChild(styleEl);
    }

    function parseStyle() {
        let result = '';
        each(style, (rules, selector) => {
            rules = rules.trim();
            result += `[${hostElCpnNameAttribute}="${cpnName}"] ${selector}{${rules}}`;
        });
        return result;
    }
}

function wrapElement(el) {
    const tagName = getParentTagName(el.tagName);
    const wrap = document.createElement(tagName);
    wrap.appendChild(el);

    return wrap;
}

function getParentTagName(tagName) {
    tagName = tagName.toUpperCase();

    switch (tagName) {
        case 'TR': return 'TABLE';
        case 'TD': return 'TR';
        default: return 'DIV';
    }
}

function parseHTML(template) {
    template = template.trim();

    const rootElNodeNameMatch = template.match(/^<(\w+)/);
    
    if (!rootElNodeNameMatch)
        throw 'Template must have a root element. \n' + template;
    
    const parentTagName = getParentTagName(rootElNodeNameMatch[1]);
    const parser = document.createElement(parentTagName);
    parser.innerHTML = template;

    if (parser.children.length !== 1)
        throw 'Template must have a root element. \n' + template;

    return parser.childNodes[0];
}

function hasRootElement(template) {
    template = template.trim();

    const rootElNodeNameMatch = template.match(/^<(\w+)/);
    
    if (!rootElNodeNameMatch)
        return false;
    
    const parentTagName = getParentTagName(rootElNodeNameMatch[1]);
    const parser = document.createElement(parentTagName);
    parser.innerHTML = template;

    if (parser.children.length !== 1)
        return false;

    return true;
}

function failsafeCall(instance, methodName, ...args) {
    if (identifier.isFunction(instance[methodName]))
        instance[methodName].call(instance, ...args);
}
