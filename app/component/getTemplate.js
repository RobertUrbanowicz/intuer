var identifier = require('../utils/identifier');
var file = require('../utils/file');

module.exports = getTemplate;


function getTemplate(config, cb) {
    if (!identifier.isUndefined(config.template)) {
        handleTemplateString(config.template, cb);
    } else if (!identifier.isUndefined(config.templatePath)) {
        handleTemplatePath(config.templatePath, config.platform, cb);
    } else if (!identifier.isUndefined(config.templateGetter)) {
        handleTemplateGetter(config.templateGetter, cb);
    } else {
        cb(null);
    }
}

function handleTemplateString(template, cb) {
    if (identifier.isString(template)) {
        cb(template);
    } else {
        throw { msg: 'template must be a string', template: template };
    }
}

function handleTemplatePath(templatePath, platform, cb) {
    if (identifier.isString(templatePath)) {
        file.get(platform, templatePath, function (template) {
            cb(template);
        });
    } else {
        throw { msg: 'templatePath must be a string', templatePath: templatePath };
    }
}

function handleTemplateGetter(templateGetter, cb) {
    if (identifier.isFunction(templateGetter)) {
        cb(templateGetter());
    } else {
        throw { msg: 'templateGetter must be a function', templateGetter: templateGetter };
    }
}