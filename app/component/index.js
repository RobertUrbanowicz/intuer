const { each } = require('../utils/common');
const object = require('../utils/objectUtils');
const { select } = require('../utils/view/selectWrap');
const createComponentInstance = require('./instance').create;

module.exports = Component;

const componentBindingDOMelementAttribute = 'component';
const componentInitializingDOMelementAttribute = 'init-component';
const hostElIdAttribute = 'data-component-id';


function Component(parentModule) {
    const definitions = {};
    const publicInstances = {};
    const activeComponents = [];

    this.parentModule = parentModule;

    this.getInstance = getInstance;
    this.define = defineComponent;
    this.addActive = addActive;
    this.removeActive = removeActive;
    this.getActive = getActive;
    this.registerPublicInstance = registerPublicInstance;
    this.getPublicInstance = getPublicInstance;
    this.getDefinition = getDefinition;
    this.getAllDefinitions = function () { return definitions };
    this.getAllPublicInstances = function () { return publicInstances };

    this.addEventBindings = addEventBindings;
    this.runComponentsByView = runComponentsByView;
    this.runAllDeclaredComponents = runAllDeclaredComponents;

    this.init = function (el) {
        runComponentsByView(el || parentModule.el);
        addEventBindings(el || parentModule.el);
    };


    /* defining component */
    function defineComponent(name, config, cpnClass) {
        config = config || {};
        config.name = name;

        const definition = definitions[name] = {
            config: config,
            class: cpnClass,
        };

        definition.repeat = repeatComponent.bind(definition);
        definition.init = initComponent.bind(definition);

        return definition;
    }

    function addActive(name) {
        if (!activeComponents.find(cpnName => cpnName === name))
            activeComponents.push(name);
    }

    function removeActive(name) {
        const index = activeComponents.findIndex(cpnName => cpnName === name);
        if (index !== -1)
            activeComponents.splice(index, 1);
    }

    function getActive() {
        return object.copy(activeComponents);
    }

    function getInstance(name) {
        const definition = getDefinition(name);
        const instance = createComponentInstance(definition.config, definition.class, parentModule);
        return instance;
    }
    
    function initComponent(initConfig) {
        const instance = createComponentInstance(this.config, this.class, parentModule);
        instance.init(initConfig);
        return instance;
    }

    function repeatComponent(el, data) {
        const definition = this; // binded in cpn definition
        const instances = [];

        let i = 0;

        each(data, function (dataEl) {
            const instance = createComponentInstance(definition.config, definition.class, parentModule);

            instance.init({
                el: el,
                data: dataEl,
                repeated: true,
                repeatIndex: i,
            });

            instances.push(instance);

            i++;
        });

        return instances;
    }

    function registerPublicInstance(instance) {
        publicInstances[instance.name] = instance;
    }

    function getPublicInstance(name, silently) {
        if (publicInstances[name]) return publicInstances[name];
        else if (!silently) throw 'There is no public instance named ' + name;
    }

    function getDefinition(name, silently) {
        if (definitions[name]) return definitions[name];
        else if (!silently) throw 'There is no component named ' + name;
    }

    function runComponentsByView(el) {
        if (!el) return;
        const elements = getComponentHostingElements(el);
        if (!elements) return;

        for (let i = 0; i < elements.length; i++) {
            const componentName = elements[i].getAttribute(componentBindingDOMelementAttribute);

            if (definitions[componentName]) {
                definitions[componentName].initializedBy = elements[i];
                runComponent(definitions[componentName], elements[i]);
            }
        }
    }

    function getComponentHostingElements(el) {
        return select('[' + componentBindingDOMelementAttribute + ']', el);
    }

    function runComponent(definition, viewElement) {
        createComponentInstance(definition.config, definition.class, parentModule)
            .init({
                el: viewElement,
            });
    }

    /* initialize component by click event in DOM */
    function addEventBindings(el) {
        if (!el) return;
        addComponentInitializingByClick(el);
    }

    function addComponentInitializingByClick(el) {
        const elements = select('[' + componentInitializingDOMelementAttribute + ']', el);

        for (let i = 0; i < elements.length; i++) {
            const componentName = elements[i].getAttribute(componentInitializingDOMelementAttribute);
            addClickHandler(elements[i], componentName);
        }

        function addClickHandler(el, componentName) {
            el.addEventListener('click', function () {
                if (definitions[componentName]) runComponent(definitions[componentName]);
                else console.warn('There is no component named ' + componentName);
            });
        }
    }

    function runAllDeclaredComponents() {
        for (const name in definitions)
            runComponent(definitions[name]);
    }

}
