const { each } = require('../utils/common');

const _listenersProp = 'data-listeners-id';

module.exports = class ModelListeners {
  constructor() {
    this.listeners = {};
    this.lastListenerId = 0;
    this.assignedElements = {};
  }

  assignToHostElement(el, listeners) {
    this.lastListenerId ++;
    this.listeners[this.lastListenerId] = listeners;
    this.assignedElements[this.lastListenerId] = el;
    el.setAttribute(_listenersProp, this.lastListenerId);
  }

  removeFromHostElement(el) {
    const idString = el.getAttribute(_listenersProp);
    const idNumber = Number(idString);
    const elementListeners = this.listeners[idNumber];

    each(elementListeners.storageListeners, (listenerObj) => {
      listenerObj.stopListening(true);
    });

    each(elementListeners.viewListeners, (input) => {
        input.stopListening();
    });

    el.removeAttribute(_listenersProp);

    delete this.listeners[idNumber];
  }

  removeAll() {
    each(this.listeners, (elementListeners) => {

      each(elementListeners.storageListeners, (listenerObj) => {
        listenerObj.stopListening(true);
      });
      
      each(elementListeners.viewListeners, (input) => {
        input.stopListening();
      });

    });

    each(this.assignedElements, (el) => {
      el.removeAttribute(_listenersProp);
    });
  }

}
