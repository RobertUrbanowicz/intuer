const createModel = require('../view/model');
const each = require('../utils/common').each;
const identifier = require('../utils/identifier');
const { toKeysArray } = require('../utils/objectUtils');
const helpers = require('./helpers');
const ModelListeners = require('./modelListeners');
const _getTemplate = require('./getTemplate');
const { select } = require('../utils/view/selectWrap');
const { selectShallow } = require('../utils/view/selectors');
const applyRepeaters = require('../view/repeat');
const bindEventsWithMethods = require('../view/events');
const Module = require('../module');
const { Storage } = require('../model/storage');


const hostElIdAttribute = 'data-component-id';
const hostElCpnNameAttribute = 'data-component-name';

const instancesByHostElId = {};
let uniqueId = 0;


module.exports = {
    create: create,
    destroyComponents: destroyComponents, // grab first lvl components and destroy them deep
};

/**
 * @param {*} config 
 * @param {Function} ComponentClass 
 * @param {Module} moduleRef 
 */
function create(config, ComponentClass, moduleRef) {
    const lifecycleEvents = helpers.createLifecycleEvents();
    const eventListeners = [];

    let instance;
    let storage;

    constructor();

    return instance;

    function constructor() {
        validateSingleton();

        instance = ComponentClass ? new ComponentClass() : {};
        instance.name = config.name;
        instance.fullName = `${moduleRef.name}.${config.name}`;
        instance.init = init;
        instance.destroy = destroy;
        instance.on = on;
        instance.listen = listen;
    }

    /**
     * @param {object} initConfig
     * @param {*} initConfig.data
     * @param {HTMLElement} initConfig.el
     * @param {boolean} initConfig.repeated
     */
    function init(initConfig) {
        instance.__id = ++ uniqueId;

        moduleRef.component.addActive(config.name);
        config.initialized = true;
        initConfig = initConfig || {};

        storage = new Storage();

        instance.__modelListeners = new ModelListeners();
        instance.__storage = storage;
        instance.model = storage.data;
        instance.onModelChange = storage.onchange;
        instance.appendType = config.appendType;
        instance.initData = initConfig.data;
        instance.repeated = initConfig.repeated;
        instance.repeatIndex = initConfig.repeatIndex;
        instance.el = getComponentElement(initConfig.el);
        instance.events = helpers.createEvents(config.events);
        instance.publicInstances = helpers.getPublicInstances(config.publicInstances, moduleRef.component);
        instance.__repeatEvents = {};

        trackComponentHostEl(instance);
        
        helpers.appendStyle(instance.fullName, config.style);

        if (config.isPublic) {
            moduleRef.component.registerPublicInstance(instance);
        }

        helpers.failsafeCall(instance, 'onInit');
        lifecycleEvents.init.trigger(instance);

        getTemplate(handleTemplate);

        return instance;
    }

    function getComponentElement(el) {
        if (identifier.isHtmlElement(el)) return el;
        return getByConfig();

        function getByConfig() {
            let result;

            if (identifier.isString(config.runIn))
                result = select(config.runIn, moduleRef.el);

            else if (identifier.isHtmlElement(config.runIn))
                result = config.runIn;

            if (identifier.isEnumerable(result))
                result = result[0];

            return result;
        }
    }

    function getTemplate(cb) {
        const tplConfig = {
            template: config.template,
            templatePath: config.templatePath,
            templateGetter: config.templateGetter,
            platform: moduleRef.app.config.platform,
        };
        _getTemplate(tplConfig, cb);
    }

    function handleTemplate(template) {
        if (identifier.isString(template))
            whenTemplateExists(template);

        const viewServicesConfig = {
            list: toKeysArray(moduleRef.app.viewService.services),
            runAll: (el, bound) => {
                return moduleRef.app.viewService.runAll(el, storage, bound);
            },
        };

        getRootOrHostEl().setAttribute('data-component-name', instance.fullName);

        const modelConfig = createModel(getRootOrHostEl(), storage, null, viewServicesConfig.list);

        instance.__modelListeners.assignToHostElement(getRootOrHostEl(), modelConfig.listeners);
        applyRepeaters(getRootOrHostEl(), storage, null, viewServicesConfig, instance);
        viewServicesConfig.runAll(getRootOrHostEl(), modelConfig.viewServices);
        instance.__rootEvents = bindEventsWithMethods(getRootOrHostEl(), instance);
        instance.elements = helpers.getUiElements(config.elements, getRootOrHostEl());

        if (instance.hasRootElement)
            attachRootElement();

        helpers.failsafeCall(instance, 'onReady');
        lifecycleEvents.ready.trigger();

        moduleRef.component.init(getRootOrHostEl());
    }
    /* end of running sequence */

    function getRootOrHostEl() {
        return instance.rootEl || instance.el;
    }

    function whenTemplateExists(template) {
        if (!instance.el)
            throwNoHostElError(template);

        instance.hasRootElement = helpers.hasRootElement(template);
        
        if (instance.hasRootElement)
            instance.rootEl = helpers.parseHTML(template);
        else
            attachWithoutRootElement(template);

        getRootOrHostEl().setAttribute(hostElIdAttribute, instance.__id);
        getRootOrHostEl().setAttribute(hostElCpnNameAttribute, instance.fullName);

        instancesByHostElId[instance.__id] = instance;
    }

    function attachWithoutRootElement(template) {
        helpers.failsafeCall(instance, 'onBeforeAttach');
        lifecycleEvents.beforeAttach.trigger(); // triggered before bindings with DOM - cpn without root element

        if (config.appendType)
            instance.el.insertAdjacentHTML(config.appendType, template);
        else
            instance.el.innerHTML = template;

        helpers.failsafeCall(instance, 'onAttach');
        lifecycleEvents.attach.trigger();
    }

    function attachRootElement() {
        helpers.failsafeCall(instance, 'onBeforeAttach');
        lifecycleEvents.beforeAttach.trigger(); // triggered after bindings with DOM - cpn with root element

        if (!instance.repeated && !config.appendType)
            instance.el.innerHTML = '';

        instance.el.insertAdjacentElement(config.appendType || 'beforeend', instance.rootEl);

        helpers.failsafeCall(instance, 'onAttach');
        lifecycleEvents.attach.trigger();
    }

    function validateSingleton() {
        if (config.initialized && config.isSingleton)
            throw 'Component ' + config.name + ' is already initialized.';
    }

    function throwNoHostElError(template) {
        throw 'Component ' + instance.fullName + ' has not a host element. Can not append template: ' + template;
    }

    
    /**
     * 
     * @param {Event} ev event object
     * @param {Function} listener
     */
    function listen(ev, listener) {
        const id = ev.listen.call(ev, listener);

        const result = {
            id: id,
            ev: ev,
            stopListening: (silently) => {
                ev.stopListening(id, silently);
            }
        };

        return eventListeners.push(result);
    }

    /**
     * 
     * @param {string} eventName 
     * @param {Function} fn
     * @description Set listener for component lifecycle events, like: init, attach, destroy
     */
    function on(eventName, fn) {
        const ev = lifecycleEvents[eventName];

        if (ev) {
            const id = ev.listen.call(ev, fn);

            return {
                eventName: eventName,
                id: id,
                off: (silently) => {
                    ev.stopListening.call(ev, id, silently);
                },
            };
        } else {
            throw 'There is no ' + eventName + ' component lifecycle event.';
        }
    }

    function destroy(config = {}) {
        moduleRef.component.removeActive(instance.name);

        getRootOrHostEl().removeAttribute(hostElIdAttribute);

        each(instance.events, function (ev) {
            ev.removeAllListeners();
        });

        each(eventListeners, function (listenerObj) {
            listenerObj.stopListening(true);
        });

        instance.__modelListeners.removeAll();

        storage.removeAllListeners();

        instance.__rootEvents.forEach(input => input.stopListening());

        each(instance.__repeatEvents, (iterationArray) => {
            iterationArray.forEach(inputsArray => {
                inputsArray.forEach(input => input.stopListening());
            });
        });

        if (!config.shallow) {
            destroyNestedComponents(getRootOrHostEl());
            if (instance.rootEl) {
                instance.rootEl.parentElement.removeChild(instance.rootEl);
            } else {
                instance.el.innerHTML = '';
            }
            lifecycleEvents.detach.trigger(instance);
        }

        helpers.failsafeCall(instance, 'onDestroy');
        lifecycleEvents.destroy.trigger(instance);

        each(lifecycleEvents, function (ev) {
            ev.removeAllListeners();
        });

        instance.destroyed = true;
    }

}

// helpers

function trackComponentHostEl(instance) {
    if (instance.el) {
        if (elementHasComponent(instance.el))
            destroyComponentOnElementAndNestedComponents(instance.el);

        else if (isNotRepetedOrFirstRepetedComponent(instance))
            destroyNestedComponents(instance.el);
    }
}

function elementHasComponent(el) {
    const elCurrentId = el.getAttribute(hostElIdAttribute);
    return instancesByHostElId[elCurrentId] ? true : false;
}

function isNotRepetedOrFirstRepetedComponent(instance) {
    return !instance.repeated || instance.repeatIndex === 0;
}

function destroyComponentOnElementAndNestedComponents(el) {
    const elCurrentId = el.getAttribute(hostElIdAttribute);
    instancesByHostElId[elCurrentId].destroy(); // destroys nested components
    delete instancesByHostElId[elCurrentId];
}

function destroyNestedComponents(el) {
    const nestedHostElements = select('[' + hostElIdAttribute + ']', el);

    each(nestedHostElements, (el) => {
        const id = el.getAttribute(hostElIdAttribute);
        instancesByHostElId[id].destroy({ shallow: true });
        delete instancesByHostElId[id];
    });
}

function destroyComponents(el) {
    const hostElements = selectShallow('[' + hostElIdAttribute + ']', el);

    each(hostElements, (el) => {
        const id = el.getAttribute(hostElIdAttribute);
        instancesByHostElId[id].destroy();
        delete instancesByHostElId[id];
    });
}
