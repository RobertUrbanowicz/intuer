App structure consists of app, modules and components.
Work on the documentation in progress.
Simple example:

```html
<!-- index.html  -->
...
<body>

<div module="module">
    <div component="cars"></div>
</div>

<!-- bundled js (e.g. using browserify) -->
<script src="bundle.js"></script>

</body>
...
```

```javascript
// index.js

const { App, Module } = require('intuer');

const app = new App({
    runIn: 'body',
    platform: 'browser',
});

const mod = new Module('module');

app.addModule(mod);

mod.component.define('cars', {
    template: `
        <h4 bind="innerText=title"></h4>

        <input bind="value=title" />

        <ul>
            <li repeat="cars" var="car" bind="innerText=car"></li>    
        </ul>

        <input bind="value=newItem" />

        <button events="click=addCar">Add a car</button>

        <p></p>

        <div json></div>
    `,
}, class {
    onInit() {
        this.model.title = 'Hello!';
        this.model.cars = [
            'Skoda',
            'Ferrari',
        ];
    }
    
    addCar() {
        this.model.cars.push(this.model.newItem);
    }
});

app.viewService.add('json', cfg => {
    const printModel = () => {
        cfg.el.innerText = JSON.stringify(cfg.model);
    };

    printModel();
    cfg.onModelChange(printModel);
});

app.init();
```

If You want to run a demo: 
1. Open root directory of the package in terminal.
2. Type `npm i && npm run bundle`.
3. Open demo/index.html file in a browser. The browser may block scripts served from file system. In this case:
    1. Open demo directory in terminal (intuer/demo)
    2. Type `npm i && npm start`
    3. Open one of the urls showed in console